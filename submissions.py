def divide(s):
    try:
        a, b = [int(el) for el in s.split()]
        s = a / b
        return s
    except Exception as e:
        return f'Error code: {e}'


if __name__ == '__main__':
    while True:
        print(divide(input()))