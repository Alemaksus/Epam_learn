# class Counter(object): <- old-style Python 2 definition

class Counter:
    """I count. That is all"""
    def __init__(self, initial=0):  # constructor
        self.value = initial        # attribute entry

    def increment(self):            # class method
        self.value += 1

    def get(self):
        return self.value           # read attribute


c = Counter(50)
c.increment()
c.get()

print(c.get())