import os


class cd:
    def __init__(self, path):
        self.path = path
        if self.path is None or os.path.isdir(self.path) == False:
            raise ValueError
        self.remember = None

    def __enter__(self):
        self.remember = os.getcwd()
        os.chdir(self.path)

    def __exit__(self, exc_type, exc_val, exc_tb):
        os.chdir(self.remember)
