
def benchmark(iters):
    def actual_decorator(func):
        import time
        """
        Cоздаём декоратор, замеряющий время выполнения функции.
        Далее мы используем его на функции, которая делает GET-запрос
        к главной странице Google. Чтобы измерить скорость, мы сначала
        сохраняем время перед выполнением обёрнутой функции,
        выполняем её, снова сохраняем текущее время и вычитаем из него начальное
        """
        def wrapper(*args, **kwargs):
            total = 0
            for i in range(iters):
                start = time.time()
                return_value = func(*args, **kwargs)
                end = time.time()
                total = total + (end-start)
            print('[*] Среднее время выполнения: {} секунд.'.format(total/iters))
            return return_value

        return wrapper
    return actual_decorator
    """
    Далее мы модифицировали наш старый декоратор таким образом, 
    чтобы он выполнял декорируемую функцию iters раз, а затем выводил 
    среднее время выполнения. Однако чтобы добиться этого, пришлось 
    воспользоваться природой функций в Python.
    Функция benchmark() на первый взгляд может показаться декоратором, 
    но на самом деле таковым не является. 
    Это обычная функция, которая принимает аргумент iters, а затем возвращает
    декоратор. В свою очередь, он декорирует функцию fetch_webpage(). 
    Поэтому мы использовали не выражение @benchmark, а @benchmark(iters=10) 
    — это означает, что тут вызывается функция benchmark() 
    (функция со скобками после неё обозначает вызов функции), 
    после чего она возвращает сам декоратор.
    """


@benchmark(iters=1)
def fetch_webpage(url):
    import requests
    web_page = requests.get(url)
    return webpage.text


webpage = fetch_webpage('https://google.com')
print(webpage)