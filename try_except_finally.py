"""
Try/Except/Finally
When attaching a finally statement to the end of a try/except, this code will be
executed after the try has been completed, regardless of exceptions.
"""

count = 0
while True:
    try:
        num = int(input("Enter an int: "))
        break
    except Exception as e:
        print(e)
    # else: # instead of next block with 'finally'
    #     print("Thank you for the integer!")
    #     break
    finally:
        count += 1
        print("Attempt #:", count)
# Enter an int: a
# invalid literal for int() with base 10: 'a'
# Attempt #: 1
# Enter an int: 3
# Attempt #: 2

"""
This might look a bit odd because the break is still inside the try. 
It’s reasonable to think that the finally would be cut short upon proper input, 
however, that’s not the case. The finally section will still execute, 
regardless of how the try is exited
"""