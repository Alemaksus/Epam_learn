def outer_func(a, b):
    def inner_func(c, d):
        return c + d
    return inner_func(a, b)
res = outer_func(5, 10)
print(res)

print('-'*50)

names = ['Alexey', 'Ivan', 'Petr']

# Imperative style code:

# for i in range(len(names)):
#     names[i] = hash(names[i])
#
# print(names)

# functional style code:

list(map(hash, names))
# or
list(map(lambda x: hash(x), names))

"""
Дается список строк, нужно посчитать кол-во вхождений определ-го слова.
Нужно вернуть сумму кол-ва вхождений слова "test"
"""
# Imperative style code:

sentences = ['test string',
             'with two test words: test and test',
             'and some without ** string']

count = 0
for sentence in sentences:
    count += sentence.count('test')

# functional style code:

sum(s.count('test') for s in sentences)

a = (n for n in range(1, 100))
print(list(a))